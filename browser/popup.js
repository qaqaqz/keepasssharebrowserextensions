var loadFiles = function(){
  browser.tabs.query({'active': true, 'lastFocusedWindow': true}).then(function (tabs) {
    loadFile("browserLib.js",function(){
      loadFile("browser.js", function(){
        window.close();
      });
    });
  });
};

var loadFile = function(file, callback){
  browser.tabs.executeScript({file:file}).then(callback).catch((a,b)=>console.log(a,a.message));
};

document.addEventListener('DOMContentLoaded', function () {
  loadFiles();
});
