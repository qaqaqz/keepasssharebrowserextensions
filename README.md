# KeepassShareBrowserExtensions #

Browser extension to integrate with the keepass share plugin

### What is this repository for? ###

* Is to generate a QRCode containing the url of the current website and a one-time pad to allow sending the credentials from the keepass share extension

### How do I get set up? ###

* TODO

### Where can I download the extensions ###

* Chrome: https://chrome.google.com/webstore/detail/keepassonetimepad/bmnkmgmlfmieoahicjgcilommidbjlbc

### Contribution guidelines ###

* Writing tests (currently there are none)
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin